Assignment 1: The below program is to guess the correct number between 1 to 100

def CheckDigitValidation(number):
    if number.isdigit():
        return True
    else:
        return False

def CheckRangeValidation(number, minRangeValue, maxRangeValue):
    if (minRangeValue <= number and number <= maxRangeValue) :
        return True
    else:
        return False

def NumberValidation(number, minRangeValue, maxRangeValue):
    if CheckDigitValidation(number) and CheckRangeValidation(number, minRangeValue, maxRangeValue)
        return True
    else:
        return False

def GuessNumber():
    MIN_NUMBER = 1
    MAX_NUMBER = 100

    expectedNumber = random.randint(MIN_NUMBER, MAX_NUMBER)
    
    isGuessed = False
    guessedNumber = input("Guess a number between 1 and 100:")
    
    totalGuesses = 0
    while not isGuessed:
        if not NumberValidation(guessedNumber, MIN_NUMBER, MAX_NUMBER):
            guessedNumber = input("I wont count this one Please enter a number between 1 to 100")
            continue
        else:
            totalGuesses += 1
            guessedNumber = int(guessedNumber)

        if guessedNumber < expectedNumber:
            guessedNumber = input("Too low. Guess again")
        elif guessedNumber > expectedNumber:
            guessedNumber = input("Too High. Guess again")
        else:
            print("You guessed it in", totalGuesses, "guesses!")
            isGuessed = True

def main():
    GuessNumber()