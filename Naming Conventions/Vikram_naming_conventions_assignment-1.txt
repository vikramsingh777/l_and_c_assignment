Assignment 1: The below program is to Roll the Dice

import random
def GenerateRandomNumber(maxValue):
    number = random.randint(1, maxValue)
    return number


def main():
    diceOutcomes = 6
    isFurtherDiceRolling = True
    while isFurtherDiceRolling:
        query = input("Ready to roll? Enter Q to Quit")
        if query.lower() != "q":
            diceOutput = GetNumberByDiceRoll(diceOutcomes)
            print("You have rolled a", diceOutput)
        else:
            isFurtherDiceRolling = False




Assignment 2: The below program is to guess the correct number between 1 to 100

def CheckNumberAndRangeValidation(number):
    if number.isdigit() and 1<= int(number) <=100:
        return True
    else:
        return False

def main():
    expectedNumber = random.randint(1,100)
    isGuessed = False
    guessedNumber = input("Guess a number between 1 and 100:")
    totalGuesses = 0
    while not isGuessed:
        if not CheckNumberAndRangeValidation(guessedNumber):
            guessedNumber = input("I wont count this one Please enter a number between 1 to 100")
            continue
        else:
            totalGuesses += 1
            guessedNumber = int(guessedNumber)

        if guessedNumber < expectedNumber:
            guessedNumber = input("Too low. Guess again")
        elif guessedNumber > expectedNumber:
            guessedNumber = input("Too High. Guess again")
        else:
            print("You guessed it in", totalGuesses, "guesses!")
            isGuessed = True




Assignment 3: The below program is to check whether the number is Armstrong number or not

def GetSumForArmstrong(number):
    # Initializing Sum and Number of Digits
    sum = 0
    totalDigits = 0

    # Calculating Number of individual digits
    remainingNumber = number
    while remainingNumber > 0:
        totalDigits = totalDigits + 1
        remainingNumber = remainingNumber // 10

    # Finding Armstrong Number
    remainingNumber = number
    for i in range(1, remainingNumber + 1):
        currentDigit = remainingNumber % 10
        sum = sum + (currentDigit ** totalDigits)
        remainingNumber //= 10
    return sum


# End of Function

# User Input
number = int(input("\nPlease Enter the Number to Check for Armstrong: "))

if (number == GetSumForArmstrong(number)):
    print("\n %d is Armstrong Number.\n" % number)
else:
    print("\n %d is Not a Armstrong Number.\n" % number)




Assignment 4: Selection Sort

function sort(array) {
  for (let i = 0; i < array.length; i++) {
    // Set default active minimum to current index.
    let indexOfMinElement = i;
    // Loop to array from the current value.
    for (let j = i + 1; j < array.length; j++) {
      // If you find an item smaller than the current active minimum,
      // make the new item the new active minimum.
      if (array[j] < array[indexOfMinElement]) {
        indexOfMinElement = j;
      }
      // Keep on looping until you've looped over all the items in the array
      // in order to find values smaller than the current active minimum.
    }
    // If the current index isn't equal to the active minimum value's index anymore
    // swap these two elements.
    if (i !== indexOfMinElement) {
      [array[i], array[indexOfMinElement]] = [array[indexOfMinElement], array[i]];
    }
  }
  return array;
}




Assignment 5: Find largest and smallest number from an array

Program to find largest and smallest number from an array

import java.util.Arrays;
/**
 * Java program to find largest and smallest number from an array.
 */
public class MaximumMinimumArrayDemo{
 
    public static void main(String args[]) {
        DisplaySmallestAndLargestNumber(new int[]{-20, 34, 21, -87, 92,
                                            Integer.MAX_VALUE});
        DisplaySmallestAndLargestNumber(new int[]{10, Integer.MIN_VALUE, -2});
        DisplaySmallestAndLargestNumber(new int[]{Integer.MAX_VALUE, 40,
                                                    Integer.MAX_VALUE});
        DisplaySmallestAndLargestNumber(new int[]{1, -1, 0});
    }
 
    public static void DisplaySmallestAndLargestNumber(int[] numbers) {
        int largestNumber = Integer.MIN_VALUE;
        int smallestNumber = Integer.MAX_VALUE;
        for (int number : numbers) {
            if (number > largestNumber) {
                largestNumber = number;
            } else if (number < smallestNumber) {
                smallestNumber = number;
            }
        }
 
        System.out.println("Given integer array : " + Arrays.toString(nums));
        System.out.println("Largest number in array is : " + largestNumber);
        System.out.println("Smallest number in array is : " + smallestNumber);
    }
}




Assignment 6: Find the floor of the expected value(mean) of the subarray from Left to Right.

Find the floor of the expected value(mean) of the subarray from Left to Right.

/*
You are given an array of n numbers and q queries. For each query you have to print the floor of the expected value(mean) of the subarray from L to R.
Inputs
First line contains two integers N and Q denoting number of array elements and number of queries.
Next line contains N space seperated integers denoting array elements.
Next Q lines contain two integers L and R(indices of the array).
Output
print a single integer denoting the answer.
*/

using System;
using System.Numerics;
class MyClass {
    static void Main(string[] args) {
       var arrayAndQueriesInfo = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            var numbers = Array.ConvertAll(Console.ReadLine().Split(' '), long.Parse);
            long[] prefixSum = new long[arrayAndQueriesInfo[0] + 1];
            prefixSum[0] = 0;
            for (int i = 1; i <= arrayAndQueriesInfo[0]; i++)
            {
                prefixSum[i] = prefixSum[i - 1] + numbers[i - 1];
            }
            for (var i = 0; i < arrayAndQueriesInfo[1]; i++)
            {
                var query = Array.ConvertAll(Console.ReadLine().Split(' '), int.
                Console.WriteLine((long)((long)(prefixSum[query[1]] - prefixSum[query[0] - 1]) / (query[1] - query[0] + 1)));
            }
    }
}
 
/* Example 
Input
5 3
1 2 3 4 5
1 3
2 4
2 5
Output2
3
3*/
