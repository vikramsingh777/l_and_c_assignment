﻿using System.Net;
using System.IO;
using System.Runtime.CompilerServices;
using System.Net.Http;
using System;
using TumblrAPI.Model;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace TumblrAPI
{
    public class Tumblr
    {
        public TumblrResponse tumblrResponse;
        public TumblrBlog tumblrBlog;

        public Tumblr()
        {
            tumblrResponse = new TumblrResponse();
            tumblrBlog = new TumblrBlog();
        }

        public TumblrResponse GetBlogInfo(TumblrQueryData tumblrQueryData)
        {
            string BlogInvalidMessage = "Blog with given blog post is not exist";
            string BlogRangeInvalidMessage = "Blog does not contain given no of post range";
            string BlogSuccessMessage = "No Error occured";

            try
            {
                if (!this.IsBlogExist(tumblrQueryData))
                {
                    tumblrResponse.Success = false;
                    tumblrResponse.Message = BlogInvalidMessage;

                    return tumblrResponse;
                }

                if (!this.IsValidNoOfPosts(tumblrQueryData))
                {
                    tumblrResponse.Success = false;
                    tumblrResponse.Message = BlogRangeInvalidMessage;

                    return tumblrResponse;
                }


                int startPostId = tumblrQueryData.PostStartId - 1;
                int totalRequiredPosts = tumblrQueryData.PostEndId - tumblrQueryData.PostStartId + 1;

                // Hit api until all requected posts is not fetched
                while (this.tumblrBlog.PostUrls.Count < totalRequiredPosts)
                {
                    int remPost = totalRequiredPosts - this.tumblrBlog.PostUrls.Count;
                    int currTotalPosts = this.tumblrBlog.PostUrls.Count;

                    JObject blogInfo = (this.GetBlogInfoInRange(tumblrQueryData.BlogName, startPostId, Math.Min(remPost, 50))).Result;

                    this.StoreBlogInfo(blogInfo);

                    //check if no extra post added then break
                    if (currTotalPosts == this.tumblrBlog.PostUrls.Count)
                    {
                        break;
                    }

                    startPostId = tumblrQueryData.PostStartId + this.tumblrBlog.PostUrls.Count + 1;
                }

                tumblrResponse.Success = true;
                tumblrResponse.Message = BlogSuccessMessage;
                tumblrResponse.TumblrBlog = tumblrBlog;
            }
            catch (Exception ex)
            {
                tumblrResponse.Success = false;
                tumblrResponse.Message = ex.Message;

                return tumblrResponse;
            }

            return tumblrResponse;
        }

        public bool IsBlogExist(TumblrQueryData tumblrQueryData)
        {
            string BlogNotExistMessage = "Blog post not exist!!";

            try
            {
                string blogName = tumblrQueryData.BlogName;
                int start = 0;
                int totalPosts = 1;

                JObject blogInfo = (this.GetBlogInfoInRange(blogName, start, totalPosts)).Result;

                blogName = (string)blogInfo["tumblelog"]["name"];
                if (string.IsNullOrEmpty(blogName) || string.IsNullOrWhiteSpace(blogName))
                {
                    return false;
                }

                return true;
            }
            catch
            {
                Console.WriteLine(BlogNotExistMessage);
            }

            return false;
        }

        public bool IsValidNoOfPosts(TumblrQueryData tumblrQueryData)
        {
            string BlogRangeErrorMessage = "Given range is out of range in blog's postrange";

            try
            {
                string blogName = tumblrQueryData.BlogName;
                int start = 0;
                int totalPosts = 1;

                JObject blogInfo = (this.GetBlogInfoInRange(blogName, start, totalPosts)).Result;

                int requiredTotalPosts = tumblrQueryData.PostEndId - tumblrQueryData.PostStartId + 1;
                int requiredBlogPostStartId = tumblrQueryData.PostStartId - 1;
                int requiredBlogPostEndId = tumblrQueryData.PostEndId - 1;

                int availableTotalPosts = (int)blogInfo["posts-total"];
                int availablePostsEndId = availableTotalPosts - 1;

                if (requiredTotalPosts > availableTotalPosts)
                {
                    return false;
                }

                if (requiredBlogPostStartId > availablePostsEndId || requiredBlogPostEndId > availablePostsEndId)
                {
                    return false;
                }

                return true;
            }
            catch
            {
                Console.WriteLine();
                Console.WriteLine(BlogRangeErrorMessage);
                Console.WriteLine();
            }

            return false;
        }

        public async Task<JObject> GetBlogInfoInRange(string blogName, int start, int totalPosts)
        {
            JObject blogInfo;

            try
            {
                string apiResponseContent;

                using (HttpClient httpClient = new HttpClient())
                {
                    string url = this.GenerateApiUrl(blogName, start, totalPosts);

                    var response = await httpClient.GetAsync(url);
                    apiResponseContent = await response.Content.ReadAsStringAsync();
                }

                string responseJsonString = this.FilterApiResponse(apiResponseContent);
                blogInfo = JObject.Parse(responseJsonString);

                return blogInfo;
            }
            catch
            {
                blogInfo = null;
            }

            return blogInfo;
        }

        public string GenerateApiUrl(string blogName, int start, int totalPosts)
        {
            string url = "https://" + blogName + ".tumblr.com/api/read/json?type=photo&start=" + start + "&num=" + totalPosts;

            return url;
        }

        public string FilterApiResponse(string ApiResponse)
        {
            // api response is combination of varible with value so remove extra information except value
            int startIndex = ApiResponse.IndexOf('{');
            int endIndex = ApiResponse.LastIndexOf('}');

            ApiResponse = ApiResponse.Substring(startIndex, (endIndex - startIndex + 1));

            return ApiResponse;
        }

        public void StoreBlogInfo(JObject blogInfo)
        {
            if (string.IsNullOrEmpty(this.tumblrBlog.Title))
            {
                this.tumblrBlog.Title = (string)blogInfo["tumblelog"]["title"];
            }

            if (string.IsNullOrEmpty(this.tumblrBlog.Name))
            {
                this.tumblrBlog.Name = (string)blogInfo["tumblelog"]["name"];
            }

            if (string.IsNullOrEmpty(this.tumblrBlog.Description))
            {
                this.tumblrBlog.Description = (string)blogInfo["tumblelog"]["description"];
            }

            if (this.tumblrBlog.TotalPosts == 0)
            {
                this.tumblrBlog.TotalPosts = (int)blogInfo["posts-total"];
            }

            var posts = blogInfo["posts"];
            foreach (var post in posts)
            {
                HashSet<string> photoUrls = new HashSet<string>();
                photoUrls.Add((string)post["photo-url-1280"]);

                var photos = post["photos"];
                foreach (var photo in photos)
                {
                    photoUrls.Add((string)photo["photo-url-1280"]);
                }

                PostImagesUrls postImagesUrls = new PostImagesUrls();
                foreach (string photoUrl in photoUrls)
                {
                    postImagesUrls.Urls.Add(photoUrl);
                }

                this.tumblrBlog.PostUrls.Add(postImagesUrls);
            }
        }

    }
}
