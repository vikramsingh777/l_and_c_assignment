﻿using System;
using TumblrAPI.Model;

namespace TumblrAPI
{
    class TumblrProgram
    {
        public static void Main(string[] args)
        {
            string AppStartingMessage = "Starting App.....";
            string AppEndingMessage = "Ending App..........";

            Console.WriteLine(AppStartingMessage);

            TumblrProgram program = new TumblrProgram();
            Tumblr tumblr = new Tumblr();

            TumblrQueryData tumblrQueryData = program.ApiQueryInputProcessor();
            if (program.IsCorrectInput(tumblrQueryData))
            {
                // hit tumblr api
                TumblrResponse tumblrResponse = tumblr.GetBlogInfo(tumblrQueryData);

                if (!tumblrResponse.Success)
                {
                    program.DisplayOutputErrorMessage(tumblrResponse.Message);
                }
                else
                {
                    program.DisplayBlogInformation(tumblrResponse.TumblrBlog);
                }
            }
            else
            {
                program.DisplayInputErrorMessages(tumblrQueryData);
            }

            Console.WriteLine(AppEndingMessage);
        }

        public TumblrQueryData ApiQueryInputProcessor()
        {
            TumblrQueryData tumblrQueryData = new TumblrQueryData();

            string BlogNameMessage = "Enter name of the blog : ";
            string BlogStartIdMessage = "Enter starting id of the post";
            string BlogEndIdMessage = "Enter Ending id of the post.....";

            Console.WriteLine(BlogNameMessage);
            tumblrQueryData.BlogName = Console.ReadLine();

            Console.WriteLine(BlogStartIdMessage);
            tumblrQueryData.PostStartId = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(BlogEndIdMessage);
            tumblrQueryData.PostEndId = Convert.ToInt32(Console.ReadLine());

            return tumblrQueryData;
        }

        public bool IsCorrectInput(TumblrQueryData tumblrQueryData)
        {
            if (tumblrQueryData.PostStartId <= 0 || tumblrQueryData.PostEndId <= 0)
            {
                return false;
            }
            else if (tumblrQueryData.PostStartId > tumblrQueryData.PostEndId)
            {
                return false;
            }

            return true;
        }

        public void DisplayInputErrorMessages(TumblrQueryData tumblrQueryData)
        {
            string StartIdErrorMessage = "Start Id is less than zero but it should be greater than zero";
            string EndIdErrorMessage = "End Id is less than zero but it should be greater than zero";
            string RangeErrorMessage = "Start id should always less than end id";

            if (tumblrQueryData.PostStartId <= 0)
            {
                Console.WriteLine(StartIdErrorMessage);
            }

            if (tumblrQueryData.PostEndId <= 0)
            {
                Console.WriteLine(EndIdErrorMessage);
            }

            if (tumblrQueryData.PostStartId > tumblrQueryData.PostEndId)
            {
                Console.WriteLine(RangeErrorMessage);
            }
        }

        public void DisplayOutputErrorMessage(string errorMessage)
        {
            Console.WriteLine(errorMessage);
        }

        public void DisplayBlogInformation(TumblrBlog blogContent)
        {
            string TitleMessage = "Title : ";
            string BlogMessage = "Blog Name : ";
            string DescriptionMessage = "Blog Description : ";
            string PostMessage = "Total no of posts : ";

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("=================================================================");

            Console.WriteLine(TitleMessage + blogContent.Title);
            Console.WriteLine(BlogMessage + blogContent.Name);
            Console.WriteLine(DescriptionMessage + blogContent.Description);
            Console.WriteLine(PostMessage + blogContent.TotalPosts);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

            for (int i = 1; i <= blogContent.PostUrls.Count; i++)
            {
                Console.WriteLine("post " + i + " image urls : ");

                var postImagesUrls = blogContent.PostUrls[i - 1];
                foreach (string imageUrl in postImagesUrls.Urls)
                {
                    Console.WriteLine(imageUrl);
                }

                Console.WriteLine();
            }
        }
    }
}
