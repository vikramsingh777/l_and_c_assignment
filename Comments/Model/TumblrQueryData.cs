﻿namespace TumblrAPI.Model
{
    public class TumblrQueryData
    {
        public string BlogName { get; set; }
        public int PostStartId { get; set; }
        public int PostEndId { get; set; }
    }
}
