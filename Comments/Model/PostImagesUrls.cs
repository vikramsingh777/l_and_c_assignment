﻿using System.Collections.Generic;

namespace TumblrAPI.Model
{
    public class PostImagesUrls
    {
        public PostImagesUrls()
        {
            Urls = new List<string>();
        }
        public List<string> Urls { get; set; }
    }
}
