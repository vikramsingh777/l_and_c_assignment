﻿namespace TumblrAPI.Model
{
    public class TumblrResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public TumblrBlog TumblrBlog { get; set; }
    }
}
