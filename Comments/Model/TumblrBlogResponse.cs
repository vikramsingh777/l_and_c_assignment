﻿using System.Collections.Generic;

namespace TumblrAPI.Model
{
    public class TumblrBlog
    {
        public TumblrBlog()
        {
            PostUrls = new List<PostImagesUrls>();
        }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int TotalPosts { get; set; }
        public List<PostImagesUrls> PostUrls { get; set; }
    }
}
