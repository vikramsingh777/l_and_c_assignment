using Xunit;
using Geolocation;
using Geolocation.Services;
using System;
using Geolocation.Models;
using System.Collections.Generic;

namespace GeolocationTest
{
    public class MapBoxLocatorTest
    {
        [Theory]
        [InlineData(75.744, 26.910703, "Amarpali Circle, Vaishali Nagar, Jaipur, Rajasthan, India")]
        public void TestGetPlaceNameByCooridnates(double longitude, double latitude, string expextedPlaceName)
        {
            IGeoLocaitonFinder locationFinder = new MapBoxGeoLocator();

            Tuple<double, double> coordinates = new Tuple<double, double>(longitude, latitude);
            string actualPlaceName = locationFinder.GetPlaceNameByCoordinates(coordinates).Result;

            Assert.True(Object.Equals(expextedPlaceName, actualPlaceName));
        }

        [Theory]
        [InlineData("Amarpali Circle, Vaishali Nagar, Jaipur, Rajasthan, India",
            75.744, 26.910703)]
        public void TestGetCoordinatesByPlaceName(string placeName, double longitude,
            double latitude)
        {
            IGeoLocaitonFinder locaitonFinder = new MapBoxGeoLocator();

            Tuple<double, double> expectedCoordinates = new Tuple<double, double>(longitude, latitude);
            List<GeolocationCoordinate> coordinates = locaitonFinder.
                                                        GetCoordinateByPlaceNameAsync(placeName).Result;

            Assert.True(Helpers.IsCoordinatesMatch(expectedCoordinates, coordinates));
        }
    }
}
