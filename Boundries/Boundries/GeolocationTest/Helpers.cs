﻿using Geolocation.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GeolocationTest
{
    class Helpers
    {
        public static bool IsCoordinatesMatch(Tuple<double, double> expectedCoordinates, 
            List<GeolocationCoordinate> coordinates)
        {
            foreach (var coordinate in coordinates)
            {
                if(coordinate.Coordinate.Item1 == expectedCoordinates.Item1 &&
                    coordinate.Coordinate.Item2 == expectedCoordinates.Item2)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
