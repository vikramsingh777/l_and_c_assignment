﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Geolocation
{
    public static class ConfigurationReader
    {   
        private static readonly IConfigurationRoot configuration = GetConfiguraitons();

        public static string ReadKey(string key)
        {
            return configuration[key];
        }

        private static IConfigurationRoot GetConfiguraitons()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                                                    .SetBasePath(Directory.GetCurrentDirectory())
                                                    .AddJsonFile(Directory.GetCurrentDirectory() + "..\\..\\..\\..\\Configuration.json")
                                                    .Build();
            return configuration;
        }
    }
}
