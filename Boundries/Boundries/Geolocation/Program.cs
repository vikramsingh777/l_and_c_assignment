﻿using System;

namespace Geolocation
{
    class Program
    {
        static void Main(string[] args)
        {
            string WelcomeMessage = "Welcome in GeoLocation application...";
            string CloseAppMessage = "Please enter any key to close the applcation...";

            Console.WriteLine(WelcomeMessage);

            GeoLocationProcessor();

            Console.WriteLine(CloseAppMessage);
            Console.ReadLine();
        }

        private static void GeoLocationProcessor()
        {
            bool IsAppClose = false;
            while (!IsAppClose)
            {
                int userChoice = GetOperationChoice();
                PerformOperationByUserChoice(userChoice);   

                bool performMoreOperation = PerformOperationAgain();
                if (!performMoreOperation)
                {
                    IsAppClose = true;
                }
            }
        }

        private static void PerformOperationByUserChoice(int userChoice)
        {
            string WrongOptionSelectionMessage = "Please choose an appropriate option....";

            GeoLocationDisplayer locationDisplayer = new GeoLocationDisplayer();
            switch (userChoice)
            {
                case 1:
                    locationDisplayer.DisplayCoordinatesByPlaceName();
                    break;
                case 2:
                    locationDisplayer.DisplayPlaceNameByCoordinates();
                    break;
                default:
                    Console.WriteLine(WrongOptionSelectionMessage);
                    break;
            }
        }

        private static int GetOperationChoice()
        {
            string ChoiceMessage = "Enter your choice to perform operation...";
            string GetCoordinatesChoiceMessage = "1. Enter 1 for getting coordiantes of the place...";
            string GetPlaceChoiceMessage = "2. Enter 2 for getting Place name from coordiantes....";

            Console.WriteLine(ChoiceMessage);
            Console.WriteLine(GetCoordinatesChoiceMessage);
            Console.WriteLine(GetPlaceChoiceMessage);

            int userChoice = Convert.ToInt32(Console.ReadLine());

            return userChoice;
        }

        private static bool PerformOperationAgain()
        {
            string RepeatOperationMessage = "Do you want to perform operation again...[y/n]";

            Console.WriteLine(RepeatOperationMessage);
            string choice = Console.ReadLine();

            return (choice == "n" ? false : true);
        }
    }
}
