﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Geolocation.Exceptions;
using Geolocation.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Geolocation.Services
{
    public class MapBoxGeoLocator : IGeoLocaitonFinder
    {
        private readonly static string accessToken = ConfigurationReader.ReadKey("MapBoxAPI:API_ACCESS_TOKEN");

        public MapBoxGeoLocator()
        {
            
        }

        public async Task<List<GeolocationCoordinate>> GetCoordinateByPlaceNameAsync(string placeName)
        {
            string InvalidPlaceNameExceptionMessage = "Enter the valid place name";
            string AccessInvalidExceptionMessage = "The response code of the Access Invalid is {0}";

            try
            {
                if (placeName is null)
                    throw new ArgumentNullException(InvalidPlaceNameExceptionMessage);

                string placeAPIEndpoint = ConfigurationReader.ReadKey("MapBoxAPI:PLACE_API");
                string apiEndpoint = string.Format(placeAPIEndpoint, placeName);
                using (HttpClient client = new HttpClient())
                {
                    string requestUrl = apiEndpoint + "?access_token=" + accessToken;
                    var response = await client.GetAsync(requestUrl);

                    if (!response.IsSuccessStatusCode)
                        throw new GeoLocationAPIAccessException(string.Format(AccessInvalidExceptionMessage, response.StatusCode));

                    string responseContent = await response.Content.ReadAsStringAsync();
                    List<GeolocationCoordinate> placeCoordinates = ExtractCoordinatesFromContent(responseContent);

                    return placeCoordinates;
                }
            }
            catch (ArgumentNullException)
            {
                throw;
            }
            catch (PlaceNotFoundException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private List<GeolocationCoordinate> ExtractCoordinatesFromContent(string content)
        {
            string InvalidContentMessage = "Enter the valid response content";
            string PlaceNotFoundExceptionMessage = "The coordinates with respect to given place name not found, please enter valid placename";

            try
            {
                if (content is null)
                    throw new ArgumentNullException(InvalidContentMessage);

                List<GeolocationCoordinate> placeCoordinates = new List<GeolocationCoordinate>();

                var jsonResponse = JObject.Parse(content);
                var locations = jsonResponse["features"];
                foreach (var location in locations)
                {
                    string placeName = location["place_name"].ToString();
                    double longitude = Convert.ToDouble(location["geometry"]["coordinates"][0].ToString());
                    double latitude = Convert.ToDouble(location["geometry"]["coordinates"][1].ToString());

                    GeolocationCoordinate locationCoordinate = new GeolocationCoordinate()
                    {
                        PlaceName = placeName,
                        Coordinate = new Tuple<double, double>(longitude, latitude)
                    };

                    placeCoordinates.Add(locationCoordinate);
                }

                if (placeCoordinates.Count == 0)
                    throw new PlaceNotFoundException(PlaceNotFoundExceptionMessage);

                return placeCoordinates;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<string> GetPlaceNameByCoordinates(Tuple<double, double> coordinates)
        {
            string AccessInvalidExceptionMessage = "The response code of the Access Invalid is {0}";

            try
            {
                string coordinateAPIEndpoint = ConfigurationReader.ReadKey("MapBoxAPI:COORDINATE_API");
                string apiEndpoint = string.Format(coordinateAPIEndpoint, coordinates.Item1, coordinates.Item2);
                using (HttpClient client = new HttpClient())
                {
                    string requestUrl = apiEndpoint + "?access_token=" + accessToken;
                    var response = await client.GetAsync(requestUrl);

                    if (!response.IsSuccessStatusCode)
                        throw new GeoLocationAPIAccessException(string.Format(AccessInvalidExceptionMessage, response.StatusCode));

                    string responseContent = await response.Content.ReadAsStringAsync();
                    string placeName = ExtractPlaceNameFromContent(responseContent, coordinates);

                    return placeName;
                }
            }
            catch (PlaceNotExistException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private string ExtractPlaceNameFromContent(string content, Tuple<double, double> coordinates)
        {
            string PlaceNotExistWithCoordinatesMessage = "No place exist with the given coordinates, place provide valid coordinates";

            string foundPlaceName = string.Empty;

            try
            {
                var jsonResponse = JObject.Parse(content);
                var locations = jsonResponse["features"];
                foreach (var location in locations)
                {
                    string placeName = location["place_name"].ToString();
                    double longitude = Convert.ToDouble(location["geometry"]["coordinates"][0].ToString());
                    double latitude = Convert.ToDouble(location["geometry"]["coordinates"][1].ToString());

                    if (locationMatch(longitude, latitude, coordinates))
                    {
                        foundPlaceName = placeName;
                        break;
                    }
                }

                if (string.IsNullOrEmpty(foundPlaceName))
                    throw new PlaceNotExistException(PlaceNotExistWithCoordinatesMessage);

                return foundPlaceName;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool locationMatch(double longitude, double latitude, Tuple<double, double> coordinates)
        {
            if (longitude == coordinates.Item1 && latitude == coordinates.Item2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
