﻿using Geolocation.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Geolocation.Services
{
    public interface IGeoLocaitonFinder
    {
        Task<List<GeolocationCoordinate>> GetCoordinateByPlaceNameAsync(string placeName);
        Task<string> GetPlaceNameByCoordinates(Tuple<double, double> coordinates);
    }
}
