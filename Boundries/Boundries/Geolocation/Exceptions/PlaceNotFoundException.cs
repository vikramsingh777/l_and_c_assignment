﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geolocation.Exceptions
{
    class PlaceNotFoundException : GeoLocationException
    {
        public PlaceNotFoundException()
        {

        }

        public PlaceNotFoundException(string message)
            : base(message)
        {

        }
    }
}
