﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geolocation.Exceptions
{
    class GeoLocationException : Exception
    {
        public GeoLocationException()
        {

        }

        public GeoLocationException(string message)
            : base(message)
        {

        }
    }
}
