﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geolocation.Exceptions
{
    class PlaceNotExistException : GeoLocationException
    {
        public PlaceNotExistException()
        {

        }

        public PlaceNotExistException(string message)
            : base(message)
        {

        }
    }
}
