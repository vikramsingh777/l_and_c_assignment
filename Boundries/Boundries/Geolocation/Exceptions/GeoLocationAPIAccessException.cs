﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geolocation.Exceptions
{
    class GeoLocationAPIAccessException : GeoLocationException
    {
        public GeoLocationAPIAccessException()
        {

        }

        public GeoLocationAPIAccessException(string message)
            : base(message)
        {

        }
    }
}
