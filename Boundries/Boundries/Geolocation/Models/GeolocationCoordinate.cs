﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geolocation.Models
{
    public class GeolocationCoordinate
    {
        public string PlaceName { get; set; }
        public Tuple<double, double> Coordinate { get; set; }
    }
}
