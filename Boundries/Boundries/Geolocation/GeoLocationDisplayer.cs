﻿using Geolocation.Models;
using Geolocation.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Geolocation.Exceptions;

namespace Geolocation
{
    class GeoLocationDisplayer
    {
        private readonly IGeoLocaitonFinder geoLocationFinder;

        public GeoLocationDisplayer()
        {
            geoLocationFinder = new MapBoxGeoLocator();
        }

        public void DisplayCoordinatesByPlaceName()
        {
            try
            {
                string placeName = GetPlaceName();
                List<GeolocationCoordinate> coordinates = geoLocationFinder.GetCoordinateByPlaceNameAsync(placeName).Result;

                DisplayCoordinates(coordinates);
            }
            catch (GeoLocationAPIAccessException ex)
            {
                Console.WriteLine("Internal Server Error, Please try after some time");
                Console.WriteLine(ex.Message);
            }
            catch (GeoLocationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void DisplayCoordinates(List<GeolocationCoordinate> coordinates)
        {
            string CoordinateDisplayFormat = "Place Name: {0} ------ Longitude: {1} ------ Latitude: {2}";

            foreach (var coordinateWithPlace in coordinates)
            {
                Console.WriteLine(string.Format(CoordinateDisplayFormat, coordinateWithPlace.PlaceName, coordinateWithPlace.Coordinate.Item1,
                    coordinateWithPlace.Coordinate.Item2));    
            }
        }

        private string GetPlaceName()
        {
            string PlaceNameInputMessage = "Enter the address of the place";

            Console.WriteLine(PlaceNameInputMessage);
            string placeName = Console.ReadLine();

            return placeName;
        }

        public void DisplayPlaceNameByCoordinates()
        {
            string DisplayPlaceNameMessage = "Place Name with respect to given coordinates is: {0}";

            try
            {
                Tuple<double, double> coordinates = GetCoordinates();
                string placeName = geoLocationFinder.GetPlaceNameByCoordinates(coordinates).Result;

                Console.WriteLine(string.Format(DisplayPlaceNameMessage, placeName));
            }
            catch (GeoLocationAPIAccessException ex)
            {
                Console.WriteLine("Internal Server Error, Please try after some time");
                Console.WriteLine(ex.Message);
            }
            catch (GeoLocationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private Tuple<double, double> GetCoordinates()
        {
            string LongitudeInputMessage = "Enter the longitude of the place...";
            string LatitudeInputMessage = "Enter the latitude of the place...";

            Console.WriteLine(LongitudeInputMessage);
            double longitude = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine(LatitudeInputMessage);
            double latitude = Convert.ToDouble(Console.ReadLine());

            Tuple<double, double> coordinates = new Tuple<double, double>(longitude, latitude);

            return coordinates;
        }
    }
}
