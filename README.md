# Tumblr API
This api provide the information about the blogs.

## Tumblr Program
This project is used to get a range of the posts for the particular blog and give information about the posts.

# Formatting guidelines for CSharp

This file contain all the necessary rule related to formatting the code, so code can be easily understable and maintainable

## Naming conventions

### Variable Naming

* Private variable name should always start with underscore ( _ ).
* Variable Name should always be in camel case.
* Const variable name should always follow the Pascal case style.

#### Examples:

```
private int _radius = GetRadius(); 
string firstName = "vikram";
const double Pi = 3.14;
const string CollegeName = "ACEIT";
```

### Class, Method, and Namespace naming

Class, Method and Namespace name should follow the Pascal case styling.

#### Examples:

```
namespace AssetManagement
{
    public class Asset 
    {
        public static void DisplayCircle(Asset asset)
        {

        }
    }
}
```

## Variable Declaration

Varible should be declare nearby it's uses. To provide better understanding and clearity varible should always declare where it will use.

#### Good Example:
```
Console.WriteLine("Display List: ");

int i = 0, n = numbers.Count;
while (i < n)
{

}
```

#### Bad Example:
```
int i = 0;
int n = numbers.Count;

Console.WriteLine("Display List: ");

while ( i < n)
{

}
```

## Sepration between concepts

Concepts should be seprated by one line with each other. Methods should also seprated by one line.

#### Example:
```
public static void CircleArea()
{
    int radius = Convert.ToInt32(Console.ReadLine());
    double Pi = 3.14;
    double circleArea = Pi * radius * radius;
}

public static void RectangleArea()
{
    int height = Convert.ToInt32(Console.ReadLine());
    int width = Convert.ToInt32(Console.ReadLine());
    double rectangleArea = height * width;
}
```

## Depedent methods should be dense

Methods that are dependent to each other should be close to each other.

#### Good Example:
```
public static void AddAsset(Asset asset)
{
    if (IsValidAsset(asset))
    {

    }
}

public static bool IsValidAsset(Asset asset)
{

}

public static DisplayAssets(List<Asset > assets)
{

}
```

#### Bad Example:
```
public static void AddAsset(Asset asset)
{
    if (IsValidAsset(asset))
    {

    }
}

public static DisplayAssets(List<Asset > assets)
{

}

public static bool IsValidAsset(Asset asset)
{

}
```

## Space between operators and operands
Operators and operands should be seprated be one space but unary operators should close to the operands.

#### Good Example:
```
int i = 0;
while(i < n) 
{

    i++;
}
```

#### Bad Example:
```
int i=0;
while(i<n)
{

    i++;
}
```

## Bracket in each block should start from new line

Start bracket of each block should start with new line 

#### Example:
```
public class Asset
{
    public static void Main()
    {

    }
}
```

## Horizontal openess
There should be horizontal openess between each argument and should be seprated by one space.

#### Good Example:
```
int rollNo = 3;
string firstName = "vikram";
```

#### Bad Example:
```
int rollNo       = 3;
string firstName = "vikram";
```

## Methods vertical ordering

Methods should be ordered in such a way so dependent Method should be above the independent method.

#### Good Example:
```
public static Asset AddAsset(Asset asset)
{
    if (IsValidAsset(asset))
    {

    }
}

public static bool IsValidAsset(Asset asset)
{

}
```
#### Bad Example:
```
public static bool IsValidAsset(Asset asset)
{

}

public static void DisplayAssets(List<Asset > assets)
{

}

public static Asset AddAsset(Asset asset)
{
    if (IsValidAsset(asset))
    {

    }
}
```

## Conditional block should consist one space before condition
Whenever we use conditional statements and loops then one space should be there before starting the conditon.

#### Good Example:
```
if (start < mid && mid < end)
{

}

for (int i = 0; i < n; i++)
{

}
```
#### Bad Example:
```
if(start < mid && mid < end)
{

}
```

## Indentation:
* Tab size should equal to the 4 space.
* Statements in each new block should start with 4 spaces

#### Good Example:
```
public class Asset
{
    public static void Main()
    {
        //start code from here

        if (condition)
        {
            //start code from here
        }
    } 
}
```

#### Bad Example:
```
public class Asset
{
public static void Main()
{
//start code from here
}
}
```