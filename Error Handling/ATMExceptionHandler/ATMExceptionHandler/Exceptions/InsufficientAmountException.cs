﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATMExceptionHandler.Exceptions
{
    class InsufficientAmountException : ATMException
    {
        public InsufficientAmountException()
        {

        }

        public InsufficientAmountException(string message) 
            : base(message)
        {

        }
    }
}
