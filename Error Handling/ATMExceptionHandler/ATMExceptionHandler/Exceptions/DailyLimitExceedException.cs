﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATMExceptionHandler.Exceptions
{
    class DailyLimitExceedException : ATMException
    {
        public DailyLimitExceedException()
        {

        }

        public DailyLimitExceedException(string message) 
            : base(message)
        {

        }
    }
}
