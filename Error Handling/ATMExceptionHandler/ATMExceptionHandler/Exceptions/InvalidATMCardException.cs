﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATMExceptionHandler.Exceptions
{
    class InvalidATMCardException : ATMException 
    {
        public InvalidATMCardException()
        {

        }

        public InvalidATMCardException(string message) 
            : base(message)
        {

        }
    }
}
