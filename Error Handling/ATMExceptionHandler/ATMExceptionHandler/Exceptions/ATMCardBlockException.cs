﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATMExceptionHandler.Exceptions
{
    class ATMCardBlockException : ATMException
    {
        public ATMCardBlockException()
        {

        }

        public ATMCardBlockException(string message) 
            : base(message)
        {

        }
    }
}
