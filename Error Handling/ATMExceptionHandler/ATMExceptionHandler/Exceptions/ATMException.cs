﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATMExceptionHandler.Exceptions
{
    class ATMException : Exception
    {
        public ATMException()
        {

        }

        public ATMException(string message)
            : base(message)
        {

        }
    }
}
