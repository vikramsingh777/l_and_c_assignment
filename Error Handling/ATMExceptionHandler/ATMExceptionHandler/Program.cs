﻿using ATMExceptionHandler.Exceptions;
using System;

namespace ATMExceptionHandler
{
    class Program
    {
        static void Main(string[] args)
        {
            string WelcomeMessage = "Application started....";
            string GoodByMessage = "Please enter a key to close the application...";

            Console.WriteLine(WelcomeMessage);

            ATM atm = new ATM();

            while (true)
            {
                ATMProcessor(atm);
            }

            Console.WriteLine(GoodByMessage);
            Console.ReadLine();
        }

        private static void ATMProcessor(ATM atm)
        {
            string AmountInputMessage = "Enter the amount to withdraw...";
            string WithdrawSuccessMessage = "Amount successfully has been withdrawed...";

            try
            {
                long atmCardNumber = ATMCardAuthenticator(atm);

                Console.WriteLine(AmountInputMessage);
                int amount = Convert.ToInt32(Console.ReadLine());

                int withdrawAmount = atm.WithdrawMoney(atmCardNumber, amount);

                Console.WriteLine(WithdrawSuccessMessage);
                
            }
            catch(InvalidATMCardException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            catch(ATMCardBlockException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            catch(InsufficientAmountException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            catch(DailyLimitExceedException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
        }

        private static long ATMCardAuthenticator(ATM atm)
        {
            string CardBlockMessage = "This card has blocked due to invalid attempts again and again";
            string CardNumberInputMessage = "Enter the ATMCardNumber";
            string CardPinInputMessage = "Enter the ATM Pin...";
            string WrongPinMessage = "Please enter the correct pin number...";

            try
            {
                Console.WriteLine(CardNumberInputMessage);
                long atmCardNumber = Convert.ToInt64(Console.ReadLine());
                bool isCardValid = atm.IsAccountExist(atmCardNumber);

                int loginTryCount = 1;
                while (loginTryCount <= 3)
                {
                    Console.WriteLine(CardPinInputMessage);
                    int atmPinNumber = Convert.ToInt32(Console.ReadLine());

                    bool isAuthenticated = atm.AuthenticateATMCard(atmCardNumber, atmPinNumber);

                    if (isAuthenticated)
                    {
                        return atmCardNumber;
                    }
                    else
                    {
                        Console.WriteLine(WrongPinMessage);
                        loginTryCount++;
                    }
                }

                atm.BlockCard(atmCardNumber);
                throw new ATMCardBlockException(CardBlockMessage);
            }
            catch(InvalidATMCardException ex)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
