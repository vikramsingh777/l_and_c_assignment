﻿using ATMExceptionHandler.Entities;
using ATMExceptionHandler.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATMExceptionHandler.Services
{
    class BankServer
    {
        private readonly List<User> users;

        public BankServer()
        {
            users = new List<User>()
            {
                new User(){ Id = 1, FirstName = "Vikram", LastName = "Singh",
                            ATMCardNumber = 1234567890123456, ATMPinNumber = 9876,
                            Amount = 100000, CurrentDayTransectAmount = 10000,
                            IsBlocked = false, AccountType = AccountTypes.Current},
                new User(){ Id = 2, FirstName = "Sumit", LastName = "Sharma",
                            ATMCardNumber = 9876543210987654, ATMPinNumber = 6789,
                            Amount = 100000, CurrentDayTransectAmount= 10000,
                            IsBlocked = false, AccountType = AccountTypes.Saving}
            };
        }

        public bool IsAccountExistWithCard(long atmCardNumber)
        {
            string CardBlockMessage = "This card is blocked...";

            foreach (var user in users)
            {
                if (user.ATMCardNumber == atmCardNumber)
                {
                    if (user.IsBlocked)
                    {
                        throw new ATMCardBlockException(CardBlockMessage);
                    }
                    else
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool AuthenticateATMCard(long atmCardNumber, int atmPinNumber)
        {
            foreach (var user in users)
            {
                if (user.ATMCardNumber == atmCardNumber && user.ATMPinNumber == atmPinNumber)
                {
                    return true;
                }
            }

            return false;
        }

        public int WithdrawMoney(long atmCardNumber, int amount)
        {
            string InsufficientAmountMessage = "Given amount is not available in you account...";
            string DailyLimitExceedMessage = "Your daily limit exceed...";

            User user = users.FirstOrDefault(x => x.ATMCardNumber == atmCardNumber);

            if (user.Amount < amount)
            {
                throw new InsufficientAmountException(InsufficientAmountMessage);
            }

            if (IsDailyLimitExceed(user, amount))
            {
                throw new DailyLimitExceedException(DailyLimitExceedMessage);
            }

            foreach (var currentUser in users)
            {
                if (currentUser.Id == user.Id)
                {
                    currentUser.Amount -= amount;
                    currentUser.CurrentDayTransectAmount += amount;
                }
            }

            return amount;
        }

        private bool IsDailyLimitExceed(User user, int amount)
        {
            int DailyLimit = 10000;

            if (user.AccountType == AccountTypes.Saving && 
                user.CurrentDayTransectAmount + amount > DailyLimit)
            {
                return true;
            }

            return false;
        }

        public void BlockCard(long atmCardNumber)
        {
            foreach (var user in users)
            {
                if (user.ATMCardNumber == atmCardNumber)
                {
                    user.IsBlocked = true;
                }
            }
        }
    }
}
