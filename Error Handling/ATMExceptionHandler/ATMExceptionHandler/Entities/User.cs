﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATMExceptionHandler.Entities
{
    class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long ATMCardNumber { get; set; }
        public int ATMPinNumber { get; set; }
        public long Amount { get; set; }
        public int  CurrentDayTransectAmount  { get; set; }
        public bool IsBlocked { get; set; }
        public AccountTypes AccountType { get; set; }
    }
}
