﻿using ShopApp.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopApp
{
    class Customer: Person
    {
        private Wallet _wallet;

        public Customer(string firstName, string lastName, double walletAmount = 0) 
            : base(firstName, lastName)
        {
            this._wallet = new Wallet(walletAmount);
        }

        public double GivePayment(double money)
        {
            try
            {
                this._wallet.Debit(money);

                return money;
            }
            catch (AmountNotFoundException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new Exception("Unknown exception generated...", ex);
            }
        }
    }
}
