﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopApp
{
    class BasicDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double InitialAmount { get; set; }
    }
}
