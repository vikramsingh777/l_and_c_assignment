﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopApp.Exceptions
{
    class AmountNotFoundException : Exception
    {
        public AmountNotFoundException()
        {

        }

        public AmountNotFoundException(string message) 
            : base(message)
        {

        }
    }
}
