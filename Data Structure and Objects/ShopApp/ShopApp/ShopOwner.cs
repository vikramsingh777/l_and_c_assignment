﻿using ShopApp.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopApp
{
    class ShopOwner : Person
    {
        private Wallet _wallet;

        public ShopOwner(string firstName, string lastName, double walletAmount = 0) 
            : base(firstName, lastName)
        {
            this._wallet = new Wallet(walletAmount);
        }

        public void GetPayment(Customer customer, double amount)
        {
            string TransactionSuccessMessage = "Amount from the customer get successfully";
            string DisplayCurrentAmount = "shop owner's current amount is:";
            string TransactionFailedMessage = "Please try again, customer do not have given amount now";

            try
            {
                this._wallet.Amount += customer.GivePayment(amount);

                Console.WriteLine(TransactionSuccessMessage);

            }
            catch(AmountNotFoundException ex)
            {
                Console.WriteLine(TransactionFailedMessage);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine(DisplayCurrentAmount);
            Console.WriteLine(this._wallet.Amount);
        }
    }
}
