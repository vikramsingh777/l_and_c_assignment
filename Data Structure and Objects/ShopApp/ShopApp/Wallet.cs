﻿using ShopApp.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopApp
{
    class Wallet
    {   
        public Wallet(double amount)
        {
            this.Amount = amount;
        }

        public double Amount { get; set; }

        public void Credit(double money)
        {
            this.Amount += money;
        }

        public void Debit(double money)
        {
            string TransactionFailedMessage = "Given amount is not available in wallet";

            if (money > this.Amount)
            {
                throw new AmountNotFoundException(TransactionFailedMessage);
            }
            else
            {
                this.Amount -= money;
            }
        }
    }
}
