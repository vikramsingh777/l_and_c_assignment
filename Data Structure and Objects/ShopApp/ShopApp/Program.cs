﻿using System;

namespace ShopApp
{
    class ShopAppProgram
    {
        static void Main(string[] args)
        {
            string WelcomeMessage = "Welcome into the shop...";
            string GoodByeMessage = "Good Bye, Enter any key to close the application....";
            string ShopOwnerDetailsMessage = "Enter the details of the shop owner: ";
            string CustomerDetailsMessage = "Enter the details of the Customer: ";

            Console.WriteLine(WelcomeMessage);

            BasicDetails ownerDetails = GetDetails(ShopOwnerDetailsMessage);
            ShopOwner owner = new ShopOwner(ownerDetails.FirstName, ownerDetails.LastName, ownerDetails.InitialAmount);

            BasicDetails customerDetails = GetDetails(CustomerDetailsMessage);
            Customer customer = new Customer(customerDetails.FirstName, customerDetails.LastName, customerDetails.InitialAmount);

            PerformTransaction(owner, customer);

            Console.WriteLine(GoodByeMessage);
            Console.ReadLine();
        }

        private static BasicDetails GetDetails(string inputMessage)
        {
            string FirstNameDisplayMessage = "Enter the First Name";
            string LastNameDisplayMessage = "Enter the Last Name";
            string WalletAmountDisplayMessage = "Enter the initial wallet amount:";

            Console.WriteLine(inputMessage);

            BasicDetails basicDetails = new BasicDetails();

            Console.WriteLine(FirstNameDisplayMessage);
            basicDetails.FirstName = Console.ReadLine();

            Console.WriteLine(LastNameDisplayMessage);
            basicDetails.LastName = Console.ReadLine();

            Console.WriteLine(WalletAmountDisplayMessage);
            basicDetails.InitialAmount = Convert.ToDouble(Console.ReadLine());

            return basicDetails;
        }

        private static void PerformTransaction(ShopOwner owner, Customer customer)
        {
            double takeAmount;
            Console.WriteLine("Enter the amount to take from customer:");
            takeAmount = Convert.ToDouble(Console.ReadLine());

            owner.GetPayment(customer, takeAmount);
        }
    }
}
